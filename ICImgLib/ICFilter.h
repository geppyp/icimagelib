//
//  ICFilter.h
//  ICImgLib
//
//  Created by Geppy Parziale on 2/22/12.
//  Copyright (c) 2012 iNVASIVECODE, Inc. All rights reserved.
//


typedef enum {
    kICFilterGabor
} ICFilterType;


typedef struct {
    size_t width;
    size_t height;
} ICKernelSize;


ICKernelSize kernelSizeMake(size_t height, size_t width);

@interface ICFilter : NSObject

@property (nonatomic, readonly) ICFilterType type;
@property (nonatomic, readonly) ICKernelSize kernelSize;
@property (nonatomic, readonly) vImage_Buffer kernel;

- (id)initWithType:(ICFilterType)filterType kernelSize:(ICKernelSize)size;

@end


