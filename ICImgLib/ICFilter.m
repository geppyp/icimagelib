//
//  ICFilter.m
//  ICImgLib
//
//  Created by Geppy Parziale on 2/22/12.
//  Copyright (c) 2012 iNVASIVECODE, Inc. All rights reserved.
//

#import "ICFilter.h"

#define rounditf(x) ((x)>=0 ? (int16_t)((x)+0.5) : (int16_t)((x)-0.5))


ICKernelSize kernelSizeMake(size_t height, size_t width)
{
    ICKernelSize size;
    size.width = width;
    size.height = height;
    return size;
}


@interface ICFilter ()
- (int16_t *)newGaborFilterForDelta:(float)angle frequency:(float)frequency sigma:(float)sigma;
@end


@implementation ICFilter
{
    vImage_Buffer _kernel;
    ICFilterType _type;
    ICKernelSize _kernelSize;
}


@synthesize type = _type;
@synthesize kernelSize = _kernelSize;
@synthesize kernel = _kernel;

- (void)dealloc
{
    ICLog;
    free(_kernel.data);
}


- (id)initWithType:(ICFilterType)filterType kernelSize:(ICKernelSize)size
{
    ICLog;
    self = [super init];
    if (self) {
        _type = filterType;
        
        size_t width;
        size_t height;
        if (size.height % 2 == 0) {
            NSLog(@"Filter height should be odd. Value corrected.");
            height = size.height + 1;
        }
        if (size.width % 2 == 0) {
            NSLog(@"Filter width should be odd. Value corrected.");
            width = size.width + 1;
        }
        _kernelSize.height = size.height;
        _kernelSize.width = size.width;
        
        if (filterType == kICFilterGabor) {
            int16_t *buffer = [self newGaborFilterForDelta:0 frequency:.001 sigma:4];
            vImage_Buffer src = { buffer, height, width, height * sizeof(int16_t) };
            _kernel = src;
        }
    }
    return self;
}


- (int16_t *)newGaborFilterForDelta:(float)angle frequency:(float)frequency sigma:(float)sigma
{   
    if (sigma > 0) {
        
        angle *= (M_PI/180.);
        
        size_t width = _kernelSize.width;
        size_t height = _kernelSize.height;
        
        int16_t *g = malloc(width * height * sizeof(int16_t));
        
        width = (width-1)/2;
        height = (height-1)/2;
    
        for (int x = -width; x <= width; x++) {
            for (int y = -height; y <= height; y++) {
                float x1 = x * cosf(angle) + y * sinf(angle);
                float y1 = - x * sinf(angle) + y * cos(angle);
                *(g + (x+width)*(2*width + 1) + (y+height)) = rounditf(cosf(2.*M_PI*frequency*x1) * expf(-.5*(x1*x1+y1*y1)/(sigma*sigma)) + 0.5);
            }
        }
        return g;
    }
    return nil;
}



//- (void)newGaborKernel
//{
//    Pixel_F *filterBuffer = [self newGaborFilterForDelta:kernelAngle frequency:kernelFrequency sigma:kernelSigma];
//    const vImage_Buffer gaborKernel = {filterBuffer, _size.height, _size.width, (_size.width*sizeof(Pixel_F))};
//    
//    NSData *gaborData = [NSData dataWithBytes:filterBuffer length:(size*size*sizeof(Pixel_F))];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *docPath = [documentsDirectory stringByAppendingPathComponent:@"standaloneFilter.raw"];
//    [gaborData writeToFile:docPath atomically:YES];
//    
//    Pixel_8 *outFilterBuffer = (Pixel_8 *)malloc( size*size*sizeof(Pixel_8) );
//    const vImage_Buffer gaborOutBuffer = { outFilterBuffer, size, size, (size*sizeof(Pixel_8)) };
//    vImageConvert_PlanarFtoPlanar8(&gaborKernel, &gaborOutBuffer, 1., 0., kvImageDoNotTile);
//    
//    CGColorSpaceRef grayColorSpace = CGColorSpaceCreateDeviceGray();
//    CGContextRef context = CGBitmapContextCreate(gaborOutBuffer.data, size, size, 8, size, grayColorSpace, kCGImageAlphaNone);
//    CGImageRef dstImageFilter = CGBitmapContextCreateImage(context);
//    CGContextRelease(context);
//    CGColorSpaceRelease(grayColorSpace);
//    
//    [self writeCGImage:dstImageFilter toFile:@"standaloneFilter.png"];
//    CGImageRelease(dstImageFilter);
//    
//    free(outFilterBuffer);
//    free(filterBuffer);
//}

@end
