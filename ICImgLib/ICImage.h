//
//  ICImage.h
//  ICImgLib
//
//  Created by Geppy Parziale on 2/1/12.
//  Copyright (c) 2012 iNVASIVECODE, Inc. All rights reserved.
//

#import "ICFilter.h"

typedef enum {
    kPlanar8,
    kARGB888,
    kRGBA888,
    kARGB8888,
    kRGBA8888,
    kPlanarF,   // not supported yet
    kARGBFFFF,  // not supported yet
    kRGBAFFFF   // not supported yet
} ICImageType;


@interface ICImage : NSObject

@property (nonatomic, readonly) ICImageType imageType;
@property (nonatomic, readonly) size_t bitsPerComponent;
@property (nonatomic, readonly) size_t bitsPerPixel;
@property (nonatomic, readonly) CGBitmapInfo bitmapInfo;
@property (nonatomic, readonly) CGImageAlphaInfo alphaInfo;
@property (nonatomic, readonly) CGColorSpaceModel colorModel;


- (id)initWithImageFromFile:(NSString *)path;
- (id)initWithCGImage:(CGImageRef)cgimage;

- (id)initImageWithImageBuffer:(const vImage_Buffer *)src 
                          type:(ICImageType)type 
              bitsPerComponent:(size_t)bpc 
                  bitsPerPixel:(size_t)bpp 
               colorSpaceModel:(CGColorSpaceModel)colorSpaceModel  
                    bitmapInfo:(CGBitmapInfo)info
                     alphaInfo:(CGImageAlphaInfo)aInfo;


- (void)writeToImageFile:(NSString *)path;
- (void)writeRAWDataFile:(NSString *)path;

- (CGImageRef)copyCGImage;

- (void)adaptiveThresholdForBlockSize:(size_t)size;

- (void)invertImage;
- (void)correctHistogram;
- (void)equalizeHistogram;
- (ICImage *)newCroppedImageFromRectangle:(CGRect)rect;


- (ICImage *)localMeanImageForBlockSize:(size_t)size;

- (ICImage *)convolveWithFilter:(ICFilter *)filter;

@end
