//
//  ICImage.m
//  ICImgLib
//
//  Created by Geppy Parziale on 2/1/12.
//  Copyright (c) 2012 iNVASIVECODE, Inc. All rights reserved.
//


#import "ICImage.h"
#import "ICImagePrivate.h"

@implementation ICImage
{
    vImage_Buffer _image;
    ICImageType _imageType;
    size_t _bitsPerComponent;
    size_t _bitsPerPixel;
    CGBitmapInfo _bitmapInfo;
    CGImageAlphaInfo _alphaInfo;
    CGColorSpaceModel _colorModel;
}


@synthesize imageType = _imageType;
@synthesize bitsPerComponent = _bitsPerComponent;
@synthesize bitsPerPixel = _bitsPerPixel;
@synthesize bitmapInfo = _bitmapInfo;
@synthesize alphaInfo = _alphaInfo;
@synthesize colorModel = _colorModel;


#pragma mark - Memory management
- (void)dealloc
{
    ICLog;
    free(_image.data);
}

#pragma mark - Initialization
// designated initializer
- (id)initImageWithImageBuffer:(const vImage_Buffer *)src 
                          type:(ICImageType)type 
              bitsPerComponent:(size_t)bpc 
                  bitsPerPixel:(size_t)bpp 
               colorSpaceModel:(CGColorSpaceModel)colorSpaceModel 
                    bitmapInfo:(CGBitmapInfo)info 
                     alphaInfo:(CGImageAlphaInfo)aInfo
{
    ICLog;
    self = [super init];
    if (self) {
        _bitsPerComponent = bpc;
        _bitsPerPixel = bpp;
        _alphaInfo = aInfo;
        _colorModel = colorSpaceModel;
        _bitmapInfo = info;
        _imageType = type;
        
        Pixel_8 *dst = (Pixel_8 *)malloc(src->height * src->rowBytes);
        memcpy(dst, src->data, src->height * src->rowBytes);
        
        const vImage_Buffer tempDst = { dst, src->height, src->width, src->rowBytes }; 
        _image = tempDst;
    }
    return self;
}


- (id)initWithImageFromFile:(NSString *)path
{
    ICLog;
    
    NSURL *url = [NSURL fileURLWithPath:path];
    CGImageSourceRef imgSrc = CGImageSourceCreateWithURL((__bridge CFURLRef)url, NULL);
    
    CGImageRef cgimage = CGImageSourceCreateImageAtIndex(imgSrc, 0, NULL);
    CFRelease(imgSrc);
    ICImage *dst = [self initWithCGImage:cgimage];
    CGImageRelease(cgimage);
    
    return dst;
}


- (id)initWithCGImage:(CGImageRef)cgimage
{
    ICLog;
    size_t w = CGImageGetWidth(cgimage);
    size_t h = CGImageGetHeight(cgimage);
    size_t bytesPerRow = CGImageGetBytesPerRow(cgimage);
    size_t bpc = CGImageGetBitsPerComponent(cgimage);
    size_t bpp = CGImageGetBitsPerPixel(cgimage);
    CGBitmapInfo info = CGImageGetBitmapInfo(cgimage);
    CGImageAlphaInfo alphaInfomation = CGImageGetAlphaInfo(cgimage);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(cgimage);
    CGColorSpaceModel colorSpaceModel = CGColorSpaceGetModel(colorSpace);
    size_t numOfComps = CGColorSpaceGetNumberOfComponents(colorSpace);
    
#if DEBUG
    switch (info) {
        case 0x1F: // 31
            NSLog(@"kCGBitmapAlphaInfoMask");
            break;
        case (1 << 8): // 256
            NSLog(@"kCGBitmapFloatComponents");
            break;
        case 0x7000: //
            NSLog(@"kCGBitmapByteOrderMask");
            break;
        case (0 << 12): // 0
            NSLog(@"kCGBitmapByteOrderDefault");
            break;
        case (1 << 12): // 4096
            NSLog(@"kCGBitmapByteOrder16Little");
            break;
        case (2 << 12): // 8192
            NSLog(@"kCGBitmapByteOrder32Little");
            break;
        case (3 << 12): // 12288
            NSLog(@"kCGBitmapByteOrder16Big");
            break;
        case (4 << 12): // 16384
            NSLog(@"kCGBitmapByteOrder32Big");
            break;
        default:
            break;
    }
    
    info = kCGBitmapByteOrderDefault;
    
    switch (alphaInfomation) {
        case 0:
            NSLog(@"kCGImageAlphaNone");
            break;
        case 1:
            NSLog(@"kCGImageAlphaPremultipliedLast");
            break;
        case 2:
            NSLog(@"kCGImageAlphaPremultipliedFirst");
            break;
        case 3:
            NSLog(@"kCGImageAlphaLast");
            break;
        case 4:
            NSLog(@" kCGImageAlphaFirst");
            break;
        case 5:
            NSLog(@"kCGImageAlphaNoneSkipLast");
            break;
        case 6:
            NSLog(@"kCGImageAlphaNoneSkipFirst");
            break;
        default:
            NSLog(@"kCGImageAlphaOnly");
            break;
    }
    
    switch (colorSpaceModel) {
        case 0:
            NSLog(@"kCGColorSpaceModelMonochrome");
            break;
        case 1:
            NSLog(@"kCGColorSpaceModelRGB");
            break;
        case 2:
            NSLog(@"kCGColorSpaceModelCMYK");
            break;
        case 3:
            NSLog(@"kCGColorSpaceModelLab");
            break;
        case 4:
            NSLog(@"kCGColorSpaceModelDeviceN");
            break;
        case 5:
            NSLog(@"kCGColorSpaceModelIndexed");
            break;
        case 6:
            NSLog(@"kCGColorSpaceModelPattern");
            break;
        default:
            NSLog(@"kCGColorSpaceModelUnknown");
            break;
    }
    
    NSLog(@"number of components: %zu", numOfComps);
#endif      
    
    void *bitmap;
    ICImageType imgType;
    CGContextRef context;
    
    if ( (alphaInfomation == kCGImageAlphaNone) && (colorSpaceModel == kCGColorSpaceModelMonochrome) && (numOfComps == 1) ) {
        
        imgType = kPlanar8;
        bitmap = (Pixel_8 *)malloc(w * h * sizeof(Pixel_8));
    
    } else if ( (alphaInfomation == kCGImageAlphaLast) && (colorSpaceModel == kCGColorSpaceModelRGB) && (numOfComps == 3) ) {
    
        imgType = kRGBA888;
        bitmap = (Pixel_8888 *)malloc(w * h * sizeof(Pixel_8888)); 
        alphaInfomation = kCGImageAlphaNoneSkipLast;
        
    } else if ( (alphaInfomation == kCGImageAlphaFirst) && (colorSpaceModel == kCGColorSpaceModelRGB) && (numOfComps == 3) ) {
        
        imgType = kARGB888;
        bitmap = (Pixel_8888 *)malloc(w * h * sizeof(Pixel_8888));
        alphaInfomation = kCGImageAlphaNoneSkipFirst;

    } else if ( (alphaInfomation == kCGImageAlphaPremultipliedLast) && (colorSpaceModel == kCGColorSpaceModelRGB) && (numOfComps == 3) ) {
        
        imgType = kRGBA8888;
        bitmap = (Pixel_8888 *)malloc(w * h * sizeof(Pixel_8888)); 
    
    } else if ( (alphaInfomation == kCGImageAlphaPremultipliedFirst) && (colorSpaceModel == kCGColorSpaceModelRGB) && (numOfComps == 3) ) {
    
        imgType = kARGB8888;
        bitmap = (Pixel_8888 *)malloc(w * h * sizeof(Pixel_8888));
    
    } else {
        NSLog(@"Unkown image format");
        return nil;
    }
    
    context = CGBitmapContextCreate(bitmap, w, h, bpc, bytesPerRow, colorSpace, alphaInfomation | info);
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), cgimage);    
    CGContextRelease(context);
    
    const vImage_Buffer src = { bitmap, h, w, bytesPerRow };
    ICImage *res = [self initImageWithImageBuffer:&src 
                                             type:imgType 
                                 bitsPerComponent:bpc 
                                     bitsPerPixel:bpp 
                                  colorSpaceModel:colorSpaceModel 
                                       bitmapInfo:info 
                                        alphaInfo:alphaInfomation];
    free(src.data);
    return res;
}


#pragma mark - Image I/O
- (void)writeToImageFile:(NSString *)path
{
    ICLog;
    CGColorSpaceRef colorSpace;
    if ( _imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    CGContextRef context = CGBitmapContextCreate(_image.data, _image.width, _image.height, _bitsPerComponent, _image.rowBytes, colorSpace, _bitmapInfo | _alphaInfo);
    
    CGImageRef dstImage = CGBitmapContextCreateImage(context);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *docPath = [documentsDirectory stringByAppendingPathComponent:[path lastPathComponent]]; 
    NSURL *outURL = [NSURL fileURLWithPath:docPath];
    
    CGImageDestinationRef dest = CGImageDestinationCreateWithURL((__bridge CFURLRef)outURL, kUTTypePNG, 1, NULL);
    CGImageDestinationAddImage(dest, dstImage, NULL);
    CGImageDestinationFinalize(dest);
    CFRelease(dest);
    CGImageRelease(dstImage);
}


- (void)writeRAWDataFile:(NSString *)path
{
    ICLog;
    NSData *srcData = [NSData dataWithBytes:_image.data length:(_image.height * _image.rowBytes)];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *docPath = [documentsDirectory stringByAppendingPathComponent:path];
    [srcData writeToFile:docPath atomically:YES];
}


//- (const vImage_Buffer)newARGB8888BufferFromImage:(CGImageRef)image
//{
//    size_t width  = CGImageGetWidth(image);
//    size_t height = CGImageGetHeight(image);
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    uint8_t *bitmap = (uint8_t *)malloc(width * height * 4 * sizeof(Pixel_8888));
//    
//    long bytesPerPixel = 4*sizeof(uint8_t);
//    long bytesPerRow = bytesPerPixel * width;
//    long bitsPerComponent = 8*sizeof(uint8_t);
//    
//    CGContextRef context = CGBitmapContextCreate(bitmap, 
//                                                 width, 
//                                                 height, 
//                                                 bitsPerComponent, 
//                                                 bytesPerRow, 
//                                                 colorSpace, 
//                                                 kCGImageAlphaPremultipliedFirst|kCGBitmapByteOrderDefault);
//    
//    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
//    CGImageRef grayImage = CGBitmapContextCreateImage(context);
//    
//    const vImage_Buffer dstBuffer = { bitmap, height, width, bytesPerRow };
//    
//    CGImageRelease(grayImage);
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    
//    return dstBuffer;
//}


//- (const vImage_Buffer)newPlanar8BufferFromImage:(CGImageRef)image
//{
//    size_t width  = CGImageGetWidth(image);
//    size_t height = CGImageGetHeight(image);
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
//    Pixel_8 *bitmap = (Pixel_8 *)malloc(width * height * sizeof(Pixel_8));
//    
//    long bytesPerPixel = 1;
//    long bytesPerRow = bytesPerPixel * width;
//    long bitsPerComponent = 8;
//    
//    CGContextRef context = CGBitmapContextCreate(bitmap, 
//                                                 width, 
//                                                 height, 
//                                                 bitsPerComponent, 
//                                                 bytesPerRow, 
//                                                 colorSpace, 
//                                                 kCGImageAlphaNone);
//    
//    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
//    CGImageRef grayImage = CGBitmapContextCreateImage(context);
//    
//    const vImage_Buffer dstBuffer = { bitmap, height, width, bytesPerRow };
//    
//    CGImageRelease(grayImage);
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    
//    return dstBuffer;
//}


- (const vImage_Buffer)newPlanarFBufferFromImage:(CGImageRef)image
{
    ICLog;
    size_t width  = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    Pixel_F *bitmap = (Pixel_F *)malloc(width * height * sizeof(Pixel_F));
    
    long bytesPerPixel = sizeof(Pixel_F);
    long bytesPerRow = bytesPerPixel * width;
    long bitsPerComponent = 8*sizeof(Pixel_F);
    
    CGContextRef context = CGBitmapContextCreate(bitmap, 
                                                 width, 
                                                 height, 
                                                 bitsPerComponent, 
                                                 bytesPerRow, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    CGImageRef grayImage = CGBitmapContextCreateImage(context);
    
    const vImage_Buffer dstBuffer = { bitmap, height, width, bytesPerRow };
    
    CGImageRelease(grayImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    return dstBuffer;
}



- (CGImageRef)copyCGImage
{
    ICLog;
    CGColorSpaceRef colorSpace;
    if ( [self imageType] == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGContextRef context = CGBitmapContextCreate(_image.data,
                                                 _image.width,
                                                 _image.height,
                                                 _bitsPerComponent,
                                                 _image.rowBytes,
                                                 colorSpace,
                                                 _bitmapInfo|_alphaInfo);
    
    CGImageRef cgimage = CGBitmapContextCreateImage(context);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return cgimage;
}

#pragma mark - Image Arithmetic
- (ICImage *)newCroppedImageFromRectangle:(CGRect)rect
{
    size_t width = _image.width;
    size_t height = _image.height;
    
    int X = (int)rect.origin.x;
    int Y = (int)rect.origin.y;
    int rows = (int)rect.size.height;
    int cols = (int)rect.size.width;
 
    if ( X+cols>width || Y+rows>height) {
        NSLog(@"Impossible to crop image. The rectangle should be contained in the original image.");
        return nil;
    }
    
    ICImage *res;
    Pixel_8 *buffer = (Pixel_8 *)_image.data;
    if (_imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        
        Pixel_8 *rectBuffer = (Pixel_8 *)malloc(rows * cols * sizeof(Pixel_8));
        
        for (int j=0; j<rows; j++) {
            for (int i=0; i<cols; i++) {
                *(rectBuffer + i + j*cols) = *(buffer + i + (j+Y-1)*width + (X-1));
            }
        }
        
        const vImage_Buffer trimmedImage = { rectBuffer, rows, cols, cols*sizeof(Pixel_8) };
        
        res = [[ICImage alloc] initImageWithImageBuffer:&trimmedImage type:_imageType bitsPerComponent:_bitsPerComponent bitsPerPixel:_bitsPerPixel colorSpaceModel:_colorModel bitmapInfo:_bitmapInfo alphaInfo:_alphaInfo];
        
        free(trimmedImage.data);
        
    } else if (_colorModel == kCGColorSpaceModelRGB) {
  
//        Pixel_8 *rectBuffer = (Pixel_8 *)malloc(rows * cols * sizeof(Pixel_8888));
        Pixel_8 *rectBuffer = (Pixel_8 *)calloc(rows * cols, sizeof(Pixel_8888));

        if (_imageType == kARGB888 || _imageType == kARGB8888 || _imageType == kRGBA888 || _imageType == kRGBA8888) {
            
            for (int j=0; j<rows; j++) {
                for (int i=0; i<cols; i=i+4) {
                    *(rectBuffer + i + j*cols) = *(buffer + i + (j+Y-1)*width + (X-1));
                    *(rectBuffer + i + 1 + j*cols) = *(buffer + i + 1 + (j+Y-1)*width + (X-1));
                    *(rectBuffer + i + 2 + j*cols) = *(buffer + i + 2 + (j+Y-1)*width + (X-1));
                    *(rectBuffer + i + 3 + j*cols) = *(buffer + i + 3 + (j+Y-1)*width + (X-1));                    
                }
            }
            
            const vImage_Buffer trimmedImage = { rectBuffer, rows, cols, cols*sizeof(Pixel_8888) };
            
            res = [[ICImage alloc] initImageWithImageBuffer:&trimmedImage type:_imageType bitsPerComponent:_bitsPerComponent bitsPerPixel:_bitsPerPixel colorSpaceModel:_colorModel bitmapInfo:_bitmapInfo alphaInfo:_alphaInfo];
            
            free(trimmedImage.data);
        }
    }
    return res;
}


//- (float)imageMax
//{
//    float max = FLT_MIN;
//
//    size_t width = _image.width;
//    size_t height = _image.height;
//    Pixel_8 *buffer = (Pixel_8 *)_image.data;
//    
//    for (int i = 0; <#condition#>; <#increment#>) {
//        <#statements#>
//    }
//    
//    
//    return max;
//}
//
//
//- (float)imageMin
//{
//    float min = FLT_MAX;
//    
//    return min;
//}
//
//
//- (float [])imageMinMax
//{
//    float minMax[2] = {0, 0};
//    
//    return minMax;
//}


#pragma mark - Image Processing

// From this paper
// http://people.scs.carleton.ca/~roth/iit-publications-iti/docs/gerh-50002.pdf
- (void)adaptiveThresholdForBlockSize:(size_t)size
{
    ICLog;
    size_t width = _image.width;
    size_t height = _image.height;
    Pixel_8 *buffer = (Pixel_8 *)_image.data;
    
    //Find the integral image
    unsigned long *integralImage = (unsigned long *)malloc(width * height * sizeof(long));
    unsigned long columnSum = 0;    
    for (int i = 0; i < width; i++) {
        columnSum = 0;
        for (int j = 0; j < height; j++) {
            long index = j*width + i; // first pixel of each column
            columnSum += *(buffer + index);
            if (i > 0)
                *(integralImage+index) = *(integralImage+(index-1)) + columnSum;
            else
                *(integralImage+index) = columnSum;
        }
    }
    
    //Threshold
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
            
            long index = j * width + i;
            
			// set the SxS region
            long x1 = i - size; 
            long x2 = i + size;
			long y1 = j - size;
            long y2 = j + size;
            
			// check the border
			if (x1 < 0) x1 = 0;
            if (x2 >= width) x2 = width - 1;
            if (y1 < 0) y1 = 0;
            if (y2 >= height) y2 = height - 1;
			
			long count = (x2-x1) * (y2-y1);
            
			// I(x,y)=s(x2,y2)-s(x1,y2)-s(x2,y1)+s(x1,x1)
			long mean = *(integralImage+y2*width+x2) - *(integralImage+y1*width+x2) - 
                            *(integralImage+y2*width+x1) + *(integralImage+y1*width+x1);
            
            mean = (int)(mean / count);
            *(buffer + index) = *(buffer + index) > mean ? 255 : 0;
		}
	}
    
	free (integralImage);
}


- (void)invertImage
{
    ICLog;
    size_t height = _image.height;
    size_t rowBytes = _image.rowBytes;
    size_t bytesCount = height*rowBytes;
    Pixel_8 *buffer = (Pixel_8 *)_image.data;
    
    if ( _imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        for (int i = 0; i < bytesCount; i++)
            *(buffer + i) = 0xFF - *(buffer + i);
    } else if (_colorModel == kCGColorSpaceModelRGB) {
        if (_imageType == kARGB888 || _imageType == kARGB8888) {
            for (int i = 0; i < bytesCount; i=i+4) {
                *(buffer + i + 1) = 0xFF - *(buffer + i + 1); // Red
                *(buffer + i + 2) = 0xFF - *(buffer + i + 2); // Green
                *(buffer + i + 3) = 0xFF - *(buffer + i + 3); // Blue
            }            
        } else if (_imageType == kRGBA888 || _imageType == kRGBA8888) {
            for (int i = 0; i < bytesCount; i=i+4) {
                *(buffer + i + 0) = 0xFF - *(buffer + i + 0); // Red
                *(buffer + i + 1) = 0xFF - *(buffer + i + 1); // Green
                *(buffer + i + 2) = 0xFF - *(buffer + i + 2); // Blue
            }
        }
    } 
}


- (void)correctHistogram
{   
    ICLog;
    size_t height = _image.height;
    size_t rowBytes = _image.rowBytes;
    size_t bytesCount = height*rowBytes;
    Pixel_8 *bitmap = (Pixel_8 *)_image.data;
    
    if ( _imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        
        // compute the cumulative histogram:
        int H[256] = { 0 };
        for (int i=0; i<bytesCount; i++)
            H[*(bitmap+i)]++;
        
        int min = 0;
        int max = 255;
        
        while (H[min] == 0) min++;
        while (H[max] == 0) max--;
        
        int levels = max - min; // number of intensity values
        for (int i = 0; i < bytesCount; i++) {
            *(bitmap+i) = (*(bitmap+i) - min) * (255/levels);
        }
        
    } else if (_colorModel == kCGColorSpaceModelRGB) {
        
        if (_imageType == kARGB888 || _imageType == kARGB8888) {
        
            // compute the cumulative histogram:
            int Hr[256] = { 0 };
            int Hg[256] = { 0 };
            int Hb[256] = { 0 };
            
            for (int i=0; i < bytesCount; i+=4) {
                Hr[*(bitmap + i + 1)]++;
                Hg[*(bitmap + i + 2)]++;
                Hb[*(bitmap + i + 3)]++;
            }
            
            int minR = 0, maxR = 255;
            while (Hr[minR] == 0) minR++;
            while (Hr[maxR] == 0) maxR--;
            int levelsR = maxR - minR; // number of red values
            
            int minG = 0, maxG = 255;
            while (Hg[minG] == 0) minG++;
            while (Hg[maxG] == 0) maxG--;
            int levelsG = maxG - minG; // number of green values
            
            int minB = 0, maxB = 255;
            while (Hb[minB] == 0) minB++;
            while (Hb[maxB] == 0) maxB--;
            int levelsB = maxB - minB; // number of blue values
            
            for (int i = 0; i < bytesCount; i+=4) {
                *(bitmap + i + 1) = (*(bitmap+i+1) - minR) * (255/levelsR);
                *(bitmap + i + 2) = (*(bitmap+i+2) - minG) * (255/levelsG);
                *(bitmap + i + 3) = (*(bitmap+i+3) - minB) * (255/levelsB);
            }
            
        } else if (_imageType == kRGBA888 || _imageType == kRGBA8888) {
        
            // compute the cumulative histogram:
            int Hr[256] = { 0 };
            int Hg[256] = { 0 };
            int Hb[256] = { 0 };
            
            for (int i=0; i < bytesCount; i+=4) {
                Hr[*(bitmap + i + 0)]++;
                Hg[*(bitmap + i + 1)]++;
                Hb[*(bitmap + i + 2)]++;
            }
            
            int minR = 0, maxR = 255;
            while (Hr[minR] == 0) minR++;
            while (Hr[maxR] == 0) maxR--;
            int levelsR = maxR - minR; // number of red values
            
            int minG = 0, maxG = 255;
            while (Hg[minG] == 0) minG++;
            while (Hg[maxG] == 0) maxG--;
            int levelsG = maxG - minG; // number of green values
            
            int minB = 0, maxB = 255;
            while (Hb[minB] == 0) minB++;
            while (Hb[maxB] == 0) maxB--;
            int levelsB = maxB - minB; // number of blue values
            
            for (int i = 0; i < bytesCount; i+=4) {
                *(bitmap + i + 0) = (*(bitmap+i+0) - minR) * (255/levelsR);
                *(bitmap + i + 1) = (*(bitmap+i+1) - minG) * (255/levelsG);
                *(bitmap + i + 2) = (*(bitmap+i+2) - minB) * (255/levelsB);
            }    
        }
    }
}


- (void)equalizeHistogram
{
    ICLog;
    size_t width = _image.width;
    size_t height = _image.height;
    size_t rowBytes = _image.rowBytes;
    size_t bytesCount = height*rowBytes;
    Pixel_8 *bitmap = (Pixel_8 *)_image.data;
    
    if (_imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        
        unsigned long hist[256] = { 0 };
        unsigned long histSum[256] = { 0 };
        
        for (int i=0; i<bytesCount; i++)
            hist[*(bitmap+i)]++;
        
        //calculate the sum of hist
        long sum = 0;
        for (int i = 0 ; i < 256 ; i++){
            sum += *(hist+i);
            *(histSum+i) = sum;
        }
        
        const float factor = 255.f/(height*width);
        
        for (int i = 0; i < bytesCount; i++) {
            unsigned char k = *(bitmap+i);
            *(bitmap+i) = *(histSum+k) * factor;
        }
        
    } else if (_colorModel == kCGColorSpaceModelRGB) {
        
        if (_imageType == kARGB888 || _imageType == kARGB8888) {
            
            unsigned long histR[256] = { 0 };
            unsigned long histSumR[256] = { 0 };
            unsigned long histG[256] = { 0 };
            unsigned long histSumG[256] = { 0 };
            unsigned long histB[256] = { 0 };
            unsigned long histSumB[256] = { 0 };
            
            for (int i=0; i<bytesCount; i+=4) {
                histR[*(bitmap+i+1)]++;
                histG[*(bitmap+i+2)]++;
                histB[*(bitmap+i+3)]++;
            }
            
            //calculate the sum of hist
            long sumR = 0, sumG = 0, sumB = 0;
            for (int i = 0 ; i < 256 ; i++){
                sumR += *(histR+i);
                *(histSumR+i) = sumR;
                sumG += *(histG+i);
                *(histSumG+i) = sumG;
                sumB += *(histB+i);
                *(histSumB+i) = sumB;
            }
            
            const float factor = 255.f/(height*width);
            
            for (int i = 0; i<bytesCount; i+=4) {
                unsigned char r = *(bitmap+i+1);
                unsigned char g = *(bitmap+i+2);
                unsigned char b = *(bitmap+i+3);
                
                *(bitmap+i+1) = *(histSumR+r) * factor;
                *(bitmap+i+2) = *(histSumG+g) * factor;
                *(bitmap+i+3) = *(histSumB+b) * factor;
            }
            
        } else if (_imageType == kRGBA888 || _imageType == kRGBA8888) {
            
            unsigned long histR[256] = { 0 };
            unsigned long histSumR[256] = { 0 };
            unsigned long histG[256] = { 0 };
            unsigned long histSumG[256] = { 0 };
            unsigned long histB[256] = { 0 };
            unsigned long histSumB[256] = { 0 };
            
            for (int i=0; i<bytesCount; i+=4) {
                histR[*(bitmap+i+0)]++;
                histG[*(bitmap+i+1)]++;
                histB[*(bitmap+i+2)]++;
            }
            
            //calculate the sum of hist
            long sumR = 0, sumG = 0, sumB = 0;
            for (int i = 0 ; i < 256 ; i++){
                sumR += *(histR+i);
                *(histSumR+i) = sumR;
                sumG += *(histG+i);
                *(histSumG+i) = sumG;
                sumB += *(histB+i);
                *(histSumB+i) = sumB;
            }
            
            const float factor = 255.f/(height*width);
            
            for (int i = 0; i<bytesCount; i+=4) {
                unsigned char r = *(bitmap+i+0);
                unsigned char g = *(bitmap+i+1);
                unsigned char b = *(bitmap+i+2);
                
                *(bitmap+i+0) = *(histSumR+r) * factor;
                *(bitmap+i+1) = *(histSumG+g) * factor;
                *(bitmap+i+2) = *(histSumB+b) * factor;
            }            
        }
    }
}


- (ICImage *)localMeanImageForBlockSize:(size_t)size
{   
    ICLog;    
    if (size % 2 == 0) {
        NSLog(@"Kernel size must be odd.");
        return nil;
    }
    
    size_t width = _image.width;
    size_t height = _image.height;
    size_t rowBytes = _image.rowBytes;
    
    ICImage *dstImage;
    
    if (_imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        
        Pixel_8 *dstBuffer = (Pixel_8 *)malloc(height * rowBytes);
        const vImage_Buffer dst = { dstBuffer, height, width, rowBytes };
        
        vImageBoxConvolve_Planar8(&_image, &dst, NULL, 0, 0, size, size, 0, kvImageEdgeExtend | kvImageDoNotTile);
        
        dstImage = [[ICImage alloc] initImageWithImageBuffer:&dst
                                                  type:_imageType 
                                      bitsPerComponent:_bitsPerComponent 
                                          bitsPerPixel:_bitsPerPixel
                                       colorSpaceModel:_colorModel 
                                            bitmapInfo:_bitmapInfo 
                                             alphaInfo:_alphaInfo];
        free(dst.data);
        
    } else if (_colorModel == kCGColorSpaceModelRGB) {
        
        Pixel_8888 *dstBuffer = (Pixel_8888 *)malloc(height * rowBytes);
        const vImage_Buffer dst = { dstBuffer, height, width, rowBytes };
        
        Pixel_8888 bgColor = {0, 0, 0, 0};
        vImageBoxConvolve_ARGB8888(&_image, &dst, NULL, 0, 0, size, size, bgColor, kvImageEdgeExtend | kvImageDoNotTile);
                
        dstImage = [[ICImage alloc] initImageWithImageBuffer:&dst
                                                  type:_imageType 
                                      bitsPerComponent:_bitsPerComponent 
                                          bitsPerPixel:_bitsPerPixel
                                       colorSpaceModel:_colorModel 
                                            bitmapInfo:_bitmapInfo
                                             alphaInfo:_alphaInfo];
        free(dst.data);        
    } 
    return dstImage;
}


- (ICImage *)copyImageAtScale:(int)scale
{
    ICLog;
    size_t newW = _image.width/scale;
    size_t newH = _image.height/scale;
    
    Pixel_8 *buffer = (Pixel_8 *)malloc(newH * newW * sizeof(Pixel_8));
    const vImage_Buffer scaled = { buffer, newH, newW, _image.rowBytes/scale };
    
    vImageScale_Planar8(&_image, &scaled, NULL, kvImageDoNotTile);
    
    ICImage *dstImage = [[ICImage alloc] initImageWithImageBuffer:&scaled 
                                                       type:_imageType 
                                           bitsPerComponent:_bitsPerComponent 
                                               bitsPerPixel:_bitsPerPixel
                                            colorSpaceModel:_colorModel 
                                                 bitmapInfo:_bitmapInfo
                                                  alphaInfo:_alphaInfo];
    return dstImage;
}

#pragma mark - Convolution
- (ICImage *)convolveWithFilter:(ICFilter *)filter
{
    ICLog;    
    size_t width = _image.width;
    size_t height = _image.height;
    size_t rowBytes = _image.rowBytes;
    
    ICImage *dstImage;
    
    if (_imageType == kPlanar8 && _colorModel == kCGColorSpaceModelMonochrome) {
        
        Pixel_8 *dstBuffer = (Pixel_8 *)malloc(height * rowBytes);
        const vImage_Buffer dst = { dstBuffer, height, width, rowBytes };
        
        vImageConvolve_Planar8(&_image, &dst, NULL, 0, 0, filter.kernel.data, filter.kernelSize.height, filter.kernelSize.width, filter.kernelSize.height*filter.kernelSize.width, 0, kvImageEdgeExtend | kvImageDoNotTile);
        
        dstImage = [[ICImage alloc] initImageWithImageBuffer:&dst
                                                        type:_imageType 
                                            bitsPerComponent:_bitsPerComponent 
                                                bitsPerPixel:_bitsPerPixel
                                             colorSpaceModel:_colorModel 
                                                  bitmapInfo:_bitmapInfo 
                                                   alphaInfo:_alphaInfo];
        free(dst.data);
        
    } else if (_colorModel == kCGColorSpaceModelRGB) {
        
        Pixel_8888 *dstBuffer = (Pixel_8888 *)malloc(height * rowBytes);
        const vImage_Buffer dst = { dstBuffer, height, width, rowBytes };
        
        Pixel_8888 bgColor = {0, 0, 0, 0};
        vImageConvolve_ARGB8888(&_image, &dst, NULL, 0, 0, filter.kernel.data, filter.kernelSize.height, filter.kernelSize.width, filter.kernelSize.height*filter.kernelSize.width, bgColor, kvImageEdgeExtend | kvImageDoNotTile);
        
        dstImage = [[ICImage alloc] initImageWithImageBuffer:&dst
                                                        type:_imageType 
                                            bitsPerComponent:_bitsPerComponent 
                                                bitsPerPixel:_bitsPerPixel
                                             colorSpaceModel:_colorModel 
                                                  bitmapInfo:_bitmapInfo
                                                   alphaInfo:_alphaInfo];
        free(dst.data);
    } 
    return dstImage;
}



@end
